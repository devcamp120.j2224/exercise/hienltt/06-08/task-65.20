package com.devcamp.pizza365.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.repository.RegionRepository;

@Service
public class RegionService {
    @Autowired
	private CountryRepository countryRepository;
    @Autowired
	private RegionRepository regionRepository;

    public CRegion createRegion(long id, CRegion cRegion) {
        Optional<CCountry> countryData = countryRepository.findById(id);
        if (countryData.isPresent()) {
            CRegion newRole = new CRegion();
            newRole.setRegionName(cRegion.getRegionName());
            newRole.setRegionCode(cRegion.getRegionCode());
            // newRole.setCountry(cRegion.getCountry());
            
            CCountry _country = countryData.get();
            newRole.setCountry(_country);
            newRole.setCountryName(_country.getCountryName());
            
            CRegion savedRole = regionRepository.save(newRole);
            return savedRole;
        }
        return null;
    }
}